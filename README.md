


* To initialize the database
```flask db init```
* To create a migration
```
flask db migrate -m "users table"
```
To upgrade to the newer database model
```
flask db upgrade
```